// http://jsfiddle.net/mekwall/up4nu/

$(document).ready(function()
{
	var home = true;
	var positionIndex = 0;
	var scrollingWindow = false;





	// BIND NAV ITEMS 
	$('.nav_items li').click(function(e)
	{
		e.preventDefault();


		positionIndex = $(this).index();
		var distance = $("#main_wrapper").innerHeight() * $(this).index();
		
		$("#main_wrapper").css('top', '-' + distance + 'px');

		fadeIn(positionIndex);

		$('.activeLink').removeClass('activeLink');
		$(this).addClass('activeLink');


		
	});


	// BIND RESIZE 
	$(window).resize(function()
	{
		var distance = $("#main_wrapper").innerHeight() * positionIndex;
		$("#main_wrapper").css('top', '-' + distance + 'px');
	});


	// HIDE HOME
	$('.start').click(function(e)
	{
		e.preventDefault();
		$('#home').css('transition','all 0.6s').css('-moz-transition','all 0.6s').css('-webkit-transition','all 0.6s');
		hideHome();
	});

	$('#home').bind('mousewheel DOMMouseScroll', function(event)
	{
		 if(scrollingWindow == false && $('.nav_items li').length-1 > positionIndex)
		 {
		 	hideHome();
		 }
	});
	function hideHome()
	{
		
		$("#home").css('top','-100%');
		home = false;
		fadeIn(0);
	}

	function showHome()
	{
		$("#home").css('top','0');
		home = true;
	}


	// BIND SCROLL 
	$('#main_wrapper').bind('mousewheel DOMMouseScroll', function(event)
	{
		
		if($(window).innerWidth() > 1024)
		{
		    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) 
		    {

		        scrollUp();
		        
		    }
		    else
		    {
		        scrollDown();    
		    }
		}

	});


	function fadeIn(eq)
	{

		$('.inner').eq(eq).find('section').css('opacity','1').css('transform','scale(1)');
	}
	
	

	/************* SCROLL DOWN *************/
	function scrollDown()
	{


		 if(scrollingWindow == false && $('.nav_items li').length-1 > positionIndex)
	        {
	        	
	        	
		        	scrollingWindow = true;
		        	positionIndex = positionIndex+1;
		        	fadeIn(positionIndex);

		        	var distance = $("#main_wrapper").innerHeight() * positionIndex;
		        	
					$("#main_wrapper").css('top', '-' + distance + 'px');
					setTimeout(function(){scrollingWindow = false;},1000);

					
					$('.activeLink').removeClass('activeLink');
					$('.nav_items li').eq(positionIndex).addClass('activeLink');
				
	        }
	}

	/************* SCROLL UP *************/

	function scrollUp()
	{
		if(scrollingWindow == false && positionIndex>=1)
		{
		        	
		 
	        	scrollingWindow = true;
	        	positionIndex = positionIndex-1;
	  			fadeIn(positionIndex);
	        	var distance = $("#main_wrapper").innerHeight() * positionIndex;
				$("#main_wrapper").css('top', '-' + distance + 'px');

				setTimeout(function(){scrollingWindow = false;},1000);

				
				$('.activeLink').removeClass('activeLink');
				$('.nav_items li').eq(positionIndex).addClass('activeLink');
					
		 }
	}


	/************* KEY PRESS *************/
	$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
        break;

        case 38: // up
        scrollUp();
        break;

        case 39: // right
        break;

        case 40: // down
        scrollDown();
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});


/************* SECTION CLICK *************/

$('.sectionHolder .openModal').click(function()
{
	$("#modal #modalInner").html('<i class="fa fa-circle-o-notch" id="modalLoader"></i>');
	var myLink = $(this).data('link');
	$("#modal #modalInner").html('<div id="modalImage"><img src="' + myLink +'"></div>');

	$("#modal").css('display','table');
	$('#modal').css('transition','all 0.2s').css('-moz-transition','all 0.2s');
	$("#modal").css('opacity','1').css('transform','scale(1)').css('-moz-transform','scale(1)');

	
	// $('#framePop').contents().scrollTop();
});

$('.closeModal').click(function()
{
	$('#modal #modalImage').scrollTop(0);
	$("#modal").css('opacity','0').css('transform','scale(0)').css('-moz-transform','scale(0)');
});



});